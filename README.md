# Dataset

This dataset is recorded using ROS-GAZEBO simulation of Remora arm
Columns of this dataset are of following field
> timestamp, joint_1, joint_2, joint_3, joint_4, joint_5, joint_ru, joint_lu, joint_rb, joint_lb, pose_x, pose_y, pose_z, orientation_x, orientation_y, orientation_z, orientation_w

joint_1 to joint_5 are of arm. joint_ru to joint_lb are of gripper.
pose_x, pose_y, pose_z are translational cooridinates and orientation_x, orientation_y, orientation_z, orientation_w are rotational coodinates of endeffector link with respect to base_link
