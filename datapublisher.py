#!/usr/bin/env python
import os
import sys
# import PyKDL as KDL
# import kdl_parser_py.urdf
import numpy as np
import rospy
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState
from remora.msg import data
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState
from control_msgs.msg import JointTrajectoryControllerState
from gazebo_msgs.srv import GetLinkState


class Data:
    def __init__(self):
        self.arm_sub = rospy.Subscriber("arm_controller/state",JointTrajectoryControllerState,self.armCallback)
        self.joint_sub = rospy.Subscriber("gripper_controller/state",JointTrajectoryControllerState,self.gripperCallback)
        rospy.wait_for_service('gazebo/get_link_state')
        self.data_pub =rospy.Publisher("remora_arm/data",data,queue_size = 10)

    def armCallback(self,msg):
        self.joint1 = msg.actual.positions[0]
        self.joint2 = msg.actual.positions[1]
        self.joint3 = msg.actual.positions[2]
        self.joint4 = msg.actual.positions[3]
        self.joint5 = msg.actual.positions[4]


    def gripperCallback(self,msg):
        self.joint_ru = msg.actual.positions[0]
        self.joint_lu = msg.actual.positions[1]
        self.joint_rb = msg.actual.positions[2]
        self.joint_lb = msg.actual.positions[3]

    def linkstateCalback(self):
        self.end_eff_state = rospy.ServiceProxy("gazebo/get_link_state",GetLinkState)
        self.pose = self.end_eff_state("link_5","base_link")
        self.pose_x = self.pose.link_state.pose.position.x
        self.pose_y = self.pose.link_state.pose.position.y
        self.pose_z = self.pose.link_state.pose.position.z
        self.orient_x = self.pose.link_state.pose.orientation.x
        self.orient_y = self.pose.link_state.pose.orientation.y
        self.orient_z = self.pose.link_state.pose.orientation.z
        self.orient_w = self.pose.link_state.pose.orientation.w

    def data_publisher(self):
        data_ = data()
        data_.data.append(self.joint1) 
        data_.data.append(self.joint2)
        data_.data.append(self.joint3)
        data_.data.append(self.joint4)
        data_.data.append(self.joint5)
        data_.data.append(self.joint_ru)
        data_.data.append(self.joint_lu)
        data_.data.append(self.joint_rb)
        data_.data.append(self.joint_lb)        
        data_.data.append(self.pose_x)
        data_.data.append(self.pose_y)
        data_.data.append(self.pose_z)
        data_.data.append(self.orient_x)
        data_.data.append(self.orient_y)
        data_.data.append(self.orient_z)
        data_.data.append(self.orient_w)
        print("printing data of joints from Arm and Gripper")
        self.data_pub.publish(data_)
        



def Main(args):
    object = Data()
    rospy.init_node("Data_Generation",anonymous=True)
    rate = rospy.Rate(50)
    while not rospy.is_shutdown():
        try:
            object.linkstateCalback()
            object.data_publisher()
        except KeyboardInterrupt:
            print("Shutting the node down")
        rate.sleep()


    
    
if __name__ == '__main__':
    Main(sys.argv)
    